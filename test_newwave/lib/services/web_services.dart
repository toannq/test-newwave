import 'package:test_newwave/constans/app_constants.dart';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

import 'package:test_newwave/model/popular.dart';

class WebServices {

  /// Api get list popular
  ///
  /// Method: [GET]
  ///
  /// Return: List<Object [Popular]>
  static Future<List<Popular>> getListPopulars({num page = 1}) async {
    final http.Response response = await http.get(
        Uri.encodeFull("${AppConstant.HOST_API}discover/movie?api_key=${AppConstant.API_KEY}&page=${page}"));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      var jsonStr = json.decode(response.body);
      var jsonEncode = json.encode(jsonStr);

      List jsonResponse = json.decode(jsonEncode)['results'];

      return jsonResponse.map((data) => Popular.fromJson(data)).toList();
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get list popular!');
    }

  }

}