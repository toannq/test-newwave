import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:test_newwave/model/popular.dart';
import 'package:test_newwave/services/web_services.dart';
import 'package:test_newwave/widgets/popular_item.dart';

class PopularScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PopularState();
}

class PopularState extends State<PopularScreen> {
  final ScrollController _controller = ScrollController();

  num page = 1;
  bool _loading = true;
  bool _canLoadMore = true;
  List<Popular> populars = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      setState(() {
        _controller.addListener(_onScroll);
        page = 1;
        getPopulars();
      });
    });
  }

  void _onScroll() {
    if (!_controller.hasClients || _loading) return;

    getPopulars();
  }

  getPopulars() async {
    WebServices.getListPopulars(page: page).then((value) {
      setState(() {
        page++;
        populars.addAll(value);
        _canLoadMore = false;

        _loading = false;
      });
    });
  }

  Future<void> _refresh() async {
    _canLoadMore = true;
    populars.clear();
    page = 1;
    await getPopulars();
  }

  Widget _buildPopularItem(BuildContext context, int index) {
    return PopularItem(populars[index]);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;

    return Scaffold(
      body: Container(
        child: CustomScrollView(
          controller: _controller,
          slivers: [
            CupertinoSliverRefreshControl(
              onRefresh: _refresh,
            ),
            SliverPadding(padding: EdgeInsets.all(10),
              sliver: SliverGrid(
                delegate: SliverChildBuilderDelegate(
                  _buildPopularItem,
                  childCount: populars.length,
                ),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: (itemWidth / itemHeight),
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                ),
              ),),
            SliverToBoxAdapter(
              child: _canLoadMore
                  ? Container(
                padding: EdgeInsets.only(bottom: 16),
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              )
                  : SizedBox(),
            ),
          ],
        ),
      ),
    );
  }
}
