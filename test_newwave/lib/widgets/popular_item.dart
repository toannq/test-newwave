import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_newwave/constans/app_constants.dart';
import 'package:test_newwave/model/popular.dart';

class PopularItem extends StatelessWidget {
  final Popular popular;

  const PopularItem(this.popular);

  Widget _buildTag() {
    return Column(
      children: [
        Row(
          children: [
            Spacer(),
            Container(
              width: 40,
              height: 40,
              margin: EdgeInsets.all(10),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Colors.blue,
                    Colors.red,
                  ],
                ),
              ),
              child: Text("${popular.voteAverage}", style: TextStyle(color: Colors.white),),
            )
          ],
        ),
        Spacer(),
        Container(
          padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
          alignment: Alignment.centerLeft,
          child: Text("${popular.releaseDate}", style: TextStyle(color: Colors.white.withOpacity(0.7), fontSize: 12,),),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
          alignment: Alignment.centerLeft,
          child: Text(popular.title.toUpperCase(), style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomLeft,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        image: DecorationImage(
            fit: BoxFit.fill,
            image: NetworkImage("${AppConstant.HOST_IMAGE}${popular.posterPath}")
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: Offset(0, 3),
            blurRadius: 4,
            spreadRadius: 0,
          )
        ],
      ),
      child: _buildTag(),
    );
  }
}